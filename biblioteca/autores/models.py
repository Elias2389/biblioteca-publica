from django.db import models

class Autor(models.Model):
	Nombre = models.CharField(max_length=25)
	Pais_de_nacimiento = models.CharField(max_length=15, blank=True)

	def __unicode__(self):
		return self.Nombre