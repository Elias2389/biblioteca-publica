from django.contrib import admin

from .models import Clasificacion

class ClasificacionAdmin(admin.ModelAdmin):
	list_display = ('Codigo_de_clasificacion', )

admin.site.register(Clasificacion, ClasificacionAdmin)