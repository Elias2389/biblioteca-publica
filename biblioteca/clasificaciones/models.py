from django.db import models

class Clasificacion(models.Model):
	Codigo_de_clasificacion = models.PositiveIntegerField()

	def __unicode__(self):
		return unicode(self.Codigo_de_clasificacion)