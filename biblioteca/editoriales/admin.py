from django.contrib import admin

from .models import Editorial

class EditorialAdmin(admin.ModelAdmin):
	list_display = ('Nombre', )


admin.site.register(Editorial, EditorialAdmin)
