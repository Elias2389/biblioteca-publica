from django.contrib import admin

from .models import Idioma

class IdiomaAdmin(admin.ModelAdmin):
	list_display = ('Idioma', )


admin.site.register(Idioma, IdiomaAdmin)