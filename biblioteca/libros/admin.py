from django.contrib import admin

from .models import Libro

class LibroAdmin(admin.ModelAdmin):
	list_display = ('Clasificacion','Titulo', 'Autor', 'Editorial', 'Materia', 'Paginas', )
	list_filter = ('Clasificacion','Titulo', 'Autor', 'Editorial', 'Materia', 'Paginas', )
	search_fields = ('Clasificacion','Titulo', 'Autor', 'Editorial', 'Materia', 'Paginas', )


admin.site.register(Libro, LibroAdmin)