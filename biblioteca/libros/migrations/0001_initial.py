# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('autores', '__first__'),
        ('idiomas', '__first__'),
        ('materias', '__first__'),
        ('clasificaciones', '__first__'),
        ('editoriales', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Libro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Titulo', models.CharField(max_length=30)),
                ('Descripcion', models.TextField(blank=True)),
                ('Paginas', models.PositiveIntegerField()),
                ('Portada', models.ImageField(upload_to=b'', verbose_name=b'Portada')),
                ('Autor', models.ForeignKey(to='autores.Autor')),
                ('Clasificacion', models.ForeignKey(to='clasificaciones.Clasificacion')),
                ('Editorial', models.ForeignKey(to='editoriales.Editorial')),
                ('Idioma', models.ManyToManyField(to='idiomas.Idioma')),
                ('Materia', models.ForeignKey(to='materias.Materia')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
