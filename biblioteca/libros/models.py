from django.db import models
from autores.models import Autor
from editoriales.models import Editorial
from idiomas.models import Idioma
from materias.models import Materia
from clasificaciones.models import Clasificacion

class Libro(models.Model):
	Titulo = models.CharField(max_length=30)
	Autor = models.ForeignKey(Autor)
	Editorial = models.ForeignKey(Editorial)
	Materia = models.ForeignKey(Materia, )
	Clasificacion = models.ForeignKey(Clasificacion)
	Idioma = models.ManyToManyField(Idioma)
	Descripcion = models.TextField(blank=True)
	Paginas = models.PositiveIntegerField()
	Portada = models.ImageField('Portada')
	#Fecha_de_publicacion = models.DateField()
	
	def __unicode__(self):
		return self.Titulo