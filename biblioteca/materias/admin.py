from django.contrib import admin

from .models import Materia

class MateriasAdmin(admin.ModelAdmin):
	search_fields = ('Nombre', )



admin.site.register(Materia, MateriasAdmin)